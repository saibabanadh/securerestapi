'use strict';

var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');

var userSchema = mongoose.Schema({
	username:{type:String,required: true},
	password:{type:String,required: true},
	email:{
		type:String,
		index: {
			unique: true
		},
		match: [/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/, 'Please enter valid email address'],
		required: true
	},
	status:{type:Boolean, default:false},
	code:{type:String, default:'0', required:false}
});

userSchema.pre('save', function(next) {
	var user = this;
    if (!user.isModified('password')) {return next();}
    bcrypt.hash(user.password, null, null, function(err, hash) {
    	if (err) {return next(err);}
        user.password = hash;
        next();
     });
});

userSchema.methods.comparePassword = function(password) {
    var user = this;
    return bcrypt.compareSync(password, user.password);
};

module.exports = mongoose.model('User', userSchema);