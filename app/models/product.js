'use strict';

var mongoose = require('mongoose');

var productSchema = mongoose.Schema({
	name:{type:String, required:true,  index: true},
	value:{type:Number, required:true},
	place:{type:String, required:true},
	unique_id:{type:String, index:{unique:true}}
});

productSchema.pre('save',function(next){
	this.unique_id = ''+this.name.replace(/ /g,'')+'_'+ Math.floor(new Date /1000);
	next();
});

module.exports = mongoose.model('Product',productSchema);