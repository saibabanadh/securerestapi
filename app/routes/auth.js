(function(){
	'use strict';
	
	//dependencies
	var express = require('express');
	//controllers
	var userCtrl = require('../controllers/user');

	//Router

	var authRouter = express.Router();

	authRouter.post('/login', userCtrl.login);
	authRouter.post('/register', userCtrl.register);
	authRouter.post('/verify', userCtrl.verifyCode);
	authRouter.post('/resetPassword', userCtrl.resetPassword);
	authRouter.post('/changePassword', userCtrl.changePassword);

	module.exports = authRouter;

})();