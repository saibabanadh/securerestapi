(function(){
	'use strict';
	
	//dependencies
	var express = require('express');

	//controllers
	var productCtrl = require('../controllers/product');
	
	//Router

	var router = express.Router();

	router.get('/products', productCtrl.getAll);
	router.post('/products', productCtrl.create);
	router.get('/products/:id', productCtrl.get);
	router.put('/products/:id', productCtrl.update);
	router.delete('/products/:id', productCtrl.remove);
	router.get('/products/names/:name', productCtrl.getByName);



	module.exports = router;

})();