'use strict';

var jwt = require('jsonwebtoken');
var User = require('../models/user');
var config = require('../../config/config');

var secret = config.secret;

module.exports = function(app){
	return  app.use(function(req,res,next){
				console.log('Url==>'+req.originalurl);
				console.log('req.body.token==>'+req.body.token);
				console.log('req.query.token==>'+req.query.token);
				console.log('req.headers.x-access-token==>'+req.headers['x-access-token']);
				
				var token = req.body.token || req.query.token || req.headers['x-access-token'];
				console.log('token==>'+token);

				if(token){	
					jwt.verify(token, secret, function(err,token){
						if(err){
							res.status(403).json({message:'Authenticated failed'});
						}else{
							req.token = token;
							next();
						}
					});
				}else{
					res.status(403).json({message:'Access token required'})
				}
		    });

}
