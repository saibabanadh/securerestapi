
'use strict';

//Models
var User = require('../models/user');
var jwt = require('jsonwebtoken');
var config = require('../../config/config');
var nodemailer = require('nodemailer').createTransport({
    service: 'Gmail',
    auth: {
        user: config.email.email,
        pass: config.email.password
    }
});
var superSecret = config.secret;
var user={};

user.login = function(req,res){
	console.log('Login');
	User.findOne({email:req.body.email})
	.select('username email password')
	.exec(function(err,user){
		if(err) 
			return err;
		if(!user){
			res.status(403).json({message:'Wrong email/password'});
		}else if(user){
			if(!(user.comparePassword(req.body.password))){
				res.status(403).json({message:'Wrong email/password'});
			}else{
				var token = jwt.sign({
					username:user.username,
					email:user.email,
				}, 
				superSecret,
				{
					expiresInMinutes:1440 //24 hrs
				});
				res.status(200).json({message:'Authenticated Successfully', token:token});
			}
		}	
	});
};

user.register = function(req,res){
	console.log('register');
    User.findOne({'email': req.body.email}, function(err, user) {
        if (!user) {
            var newUser = new User();
            newUser.username = req.body.username;
            newUser.email = req.body.email;
            newUser.password = req.body.password;
            newUser.code = Math.floor(Math.random() * 899999 + 100000);
            newUser.status = 'unverified';
            newUser.save(function(err, user) {
                if (err) {
                    console.log(err);
                    return res.status(500).json(err);
                }
                console.log('unverified record created');
                var mailOptions = {
                            from: 'SecureRESTAPI ✔'+'<'+config.email.email+'>', // sender address
                            to: req.body.email, // list of receivers
                            subject: 'SecureRESTAPI : Secret Code', // Subject line
                            text: 'Hello ', // plaintext body
                            html: 'Please enter this code to complete registration:' + user.code + " "
                };
                nodemailer.sendMail(mailOptions, function(error, info) {
                    if (error) {
                        return console.log(error);
                    }
                    console.log('Message sent: ' + info.response);
                });
                return res.status(200).json({
                    success: true,
                    message: 'Please check your mail and enter code below '
                });
            });
        } else {
            console.log(user);
            res.status(403).json({success:false, message:'User already exists, please login or reset password'})
        }
    });
};

user.verifyCode = function(req,res){
    console.log('verifyCode');
        User.findOne({'email': req.body.email}, function(err, user) {
        if (!user) {
            return res.status(404).json({
                'success': false,
                'message': 'user does not exist',
                'token': token
            });
        }
        if (user.code === req.body.code) {
            console.log('code matched');
            try {
                user.status = 'verified';
                user.code = '0';
                user.save(function(err) {
                    if (err) {
                        return res.status(500).json({
                            success: false,
                            message: err
                        });
                    } else {
                        var token = jwt.sign({
                            username: user.username,
                            email: user.email
                        }, superSecret, {
                                        expiresInMinutes: 1440 // expires in 24 hours
                                    });
                        return res.status(200).json({
                            success: true,
                            message: 'user is successfully registed',
                            token: token
                        });
                    }
                });
            } catch (err1) {
                res.status(500).json({
                    success: false,
                    message: 'Invalid request ,internal server error'
                });
            } finally {
                //nothing for now
            }
        } else {
            res.status(500).json({
                success: false,
                message: 'Invalid request , code does not match'
            });
        }
    });
};

user.resetPassword = function(req,res){
	console.log('resetpassword');
	User.findOne({'email': req.body.email}, function(err, user) {
		if (!user) {
			res.status(404).json({
				success: false,
				message: 'Email does not exist, please create a new account'
			});
		} else {
			user.code = Math.floor(Math.random() * 899999 + 100000);
			var mailOptions = {
                        from: 'SecureRESTAPI ✔'+'<'+config.email.email+'>', // sender address
                        to: req.body.email, // list of receivers
                        subject: 'SecureRESTAPI : Password Reset', // Subject line
                        text: 'Hello, please use this code: ' + user.code, // plaintext body
                        html: '<a href="http://localhost:8080/v1/auth/changePassword/' + user.code + '">Reset Password</a>'
            };
            user.status = 'unverified';
            user.save(function(err) {
            	if (err) {
            		return res.status(500).json({
            			success: false,
            			message: err
            		});
            	}
            });
            nodemailer.sendMail(mailOptions, function(error, info) {
            	if (error) {
            		return console.log(error);
            	}
            	console.log('Message sent: ' + info.response);
            });
            res.status(200).json({
            	success: true,
            	message: 'Please check your mail'
            });
        }
    });
};


user.changePassword = function(req,res){
	console.log('changepassword');
	User.findOne({'email': req.body.email}, function(err, user) {
		if (!user) {
			res.status(404).json({
				success: false,
				message: 'Invalid request , user does not exist'
			});
		} else {
			if (user.code === req.body.code) {
				try {
					user.password = req.body.password;
					user.code = '0';
					user.save(function(err) {
						if (err) {
							return res.status(500).json({
								success: false,
								message: err
							});
						} else {
							return res.status(200).json({
								success: true,
								message: 'password changed'
							});
						}
					});
				} catch (err1) {
					res.status(500).json({
						success: false,
						message: 'Invalid request , code does not match'
					});
				} finally {
                    //nothing for now
                }
            } else {
            	res.status(500).json({
            		success: false,
            		message: 'Invalid request , code does not match'
            	});
            }
        }
    });
};

module.exports = user;

