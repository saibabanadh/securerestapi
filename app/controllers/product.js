
'use strict';

//Models
var Product = require('../models/product');
var product={};
product.getAll = function(req,res){
	Product.find({},function(err,products){
		if(err) return err;
		res.status(200).json({data:products});
	})
};
product.create = function(req,res){
	var product = new Product();

	product.name = req.body.name;
	product.value = req.body.value;
	product.place = req.body.place;

	product.save(function(err, success){
	    if(err){
	      console.log(err);
	      return res.status(500).json({error:err});
	    }else{      
	      console.log('Product is Successfully Created.');
	      return res.status(201).json({product:product, message:'Product is Successfully Created.'})
	    } 
	}); 
};

product.get = function(req,res){
	Product.findOne({_id:req.params.id},function(err,product){
		if(err) return err;

		if(product == null){
			return res.status(404).json({message:'Not Found'});
		}
		res.status(200).json({product:product});
	})
};
product.getByName = function(req,res){
	Product.findOne({name:req.params.name},function(err,product){
		if(err) return err;

		if(product == null){
			return res.status(404).json({message:'Not Found'});
		}
		res.status(200).json({product:product});
	})
};

product.update = function(req,res){
	Product.findOne({_id:req.params.id},function(err,product){
		if(err) return err;
		if(product == null){
			return res.status(404).json({message:'Not Found'});
		}else{
			product.name = req.body.name;
			product.value = req.body.value;
			product.place = req.body.place;
		}
		product.save(function(err){
			if(err) return err;
			res.status(200).json({product:product, message:'Product has been updated'});
		});
	})
};

product.remove = function(req,res){
	Product.findByIdAndRemove( req.params.id,function(err, product){
    if(err){
      console.log("errrr...");
      return err;
    }
    res.status(200).json({message:'Product has been deleted'});
}); 
};

module.exports = product;

