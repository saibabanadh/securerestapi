(function(){
	'use strict';
	var fs = require('fs');
	var obj = JSON.parse(fs.readFileSync('./private.json', 'utf8'));
	
	var config ={
		'googleAuth' : {
	        'clientID'      : obj.googleAuth.clientID,
	        'clientSecret'  : obj.googleAuth.clientSecret,
	        'callbackURL'   : obj.googleAuth.callbackURL
	    },
	    'db':obj.db,
	    'port':obj.port,
	    'testurl':obj.testurl,
	    'secret':obj.secret,
	    'email':{
	    	'email':obj.email.email,
	    	'password':obj.email.password
	    }
	}

	module.exports = config;

})();