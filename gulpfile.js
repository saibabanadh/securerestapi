
(function() {
    'use strict';

    var gulp = require('gulp'),
        nodemon = require('gulp-nodemon'),
        jshint = require('gulp-jshint'),
        stylish = require('jshint-stylish'),
        gutil = require('gulp-util');
    gulp.task('lint', function() {
        gulp.src(['./*.js', './app/**/*.js', './config/**/*.js'])
            .pipe(jshint()).pipe(jshint.reporter(stylish,{ verbose: true }));
    });
    gulp.task('nodemon', function(cb) {
        var started = false;
        return nodemon({
            script: 'server.js',
            tasks: ['lint']
        }).on('start', function() {
            // to avoid nodemon being started multiple times
            if (!started) {
                cb();
                started = true;
            }
        });
    });
    gulp.task('default', ['nodemon'], function() {
    });
}());