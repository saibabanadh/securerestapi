(function(){
	'use strict';

	//dependencies
	var express = require('express'),
		app = express(),
		bodyParser = require('body-parser'),
		//cookieParser = require('cookie-parser'),
		//expressSession = require('express-session'),
		mongoose = require('mongoose'),
		//mongoStore = require('connect-mongo')({session:expressSession});
	
	//local dependencies -------------------------------------------------------
	var config = require('./config/config'),
		port = process.env.PORT || config.port;

	
	//db config -------------------------------------------------------
	var options = {
	  db: { 
	  	native_parser: true 
	  },
	  server: { 
	  	poolSize: 5 ,
	  	socketOptions:{ 
	  		keepAlive: 1 
	  	}
	  }
	}
	mongoose.connect(config.db, options);


	

	//middleware --------------------------------------------------
	//app.use(cookieParser());
	app.use(bodyParser.urlencoded({'extended':true}));
	app.use(bodyParser.json());
	app.use(bodyParser.json({type:'application/vnd.api+json'}));


	//security -------------------------------------------------------

	app.use(function(req, res, next) {
	  res.setHeader('Access-Control-Allow-Origin', '*');
	  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
	  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type, Authorization' );
	  next();
	});

	//session -------------------------------------------------------

	// app.use(expressSession({store: new mongoStore({url: config.db,collection : 'sessions'}),
	//   name:'securerestapi',
	//   secret: 'IamtheOnewhoisnotlikeOthers',
	//   cookie: {
	//   	httpOnly: true,secure: true,maxAge:2628000000
	//   },
	//   saveUninitialized: true,
	//   resave: true
	// }));

	//Authentication

	app.use('/v1/auth', require('./app/routes/auth'));

	require('./app/middleware/authenticate')(app);

	//routing -------------------------------------------------------
	app.use('/v1/api', require('./app/routes/routes'));
	
	app.use('*', function(req, res) {
	    res.status(404).json({error:"requested url: "+req.url+ ' is Not Found'});
	});

	//exceptions -------------------------------------------------------
	process.on('uncaughtException', function(err) {
	  console.log(err);
	});


	//server -------------------------------------------------------
	app.listen(port,function(){
		console.log("Server is running on port "+config.port);
	});

})();


