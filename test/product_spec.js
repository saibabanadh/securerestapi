var frisby=require('frisby');
var config= require('../config/config');



frisby.create('Post the products')
	.post(config.testurl+'/api/v1/products',{ 
		name:'MotoX',
		value:20000,
		place:'Bangalore'
	})
	.expectStatus(201)
	.expectHeaderContains('content-type', 'application/json')
	.expectJSON('product',{
		name:'MotoX',
		value:20000,
		place:'Bangalore'
	})
	.toss();

frisby.create('Get the products')
  .get(config.testurl+'/api/v1/products')
  .expectStatus(200)
  .expectHeaderContains('content-type', 'application/json')
  .inspectJSON()
  .expectJSONTypes('data.0', {
    name: String,
    place:String
  })
  .toss();